@extends('layouts.app')

@section('content')
    <h1></h1>
    <h1>Welcome to Todo List</h1>
    <p>Find below list of action items and their due date</p>
    @if (count($todos) > 0)
        @foreach ($todos as $todo)
            <div class="card m-2">
                <h4><a href="todo/{{ $todo->id }}">{{ $todo->title }}</a></h4>
                <span class="badge badge-danger">{{ $todo->due }}</span>
            </div>
        @endforeach
    @endif
@endsection